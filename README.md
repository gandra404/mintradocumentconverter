# Mintra PPT Converter

Web Service for converting Powerpoint presentation files into:  
- SWF  
- HTML  

## Administration Notes ##

This will not work out of box in development environment with default settings with IIS express.
In order to work this should be configured to use Local IIS Web Server.  
**Important!**  
IIS Manager application pool should be configured:  
- Managade Pipeline Mode: Integrated   
- Process Model/Identity: LocalSystem  
- .NET Framework Version: v4.0  
[http://i.imgur.com/KuWrSmO.png](http://i.imgur.com/KuWrSmO.png "http://i.imgur.com/KuWrSmO.png")


