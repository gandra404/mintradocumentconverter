﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Services;
using MintraDocumentConverterCore;
using NLog;

namespace MintraDocumentConverterService
{
    /// <summary>
    /// Web service for converting powerpoint presentation into HTML or SWF
    /// </summary>
    [WebService(Namespace = "http://mintra.no/webservice/converters/ppt/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class PptConverterWebService : System.Web.Services.WebService
    {

        Logger Logger = LogManager.GetCurrentClassLogger();

        [WebMethod]
        public Response Convert(string outputFormat, string pptPath)
        {
            Logger.Info("Receiving request to convert file {0} into {1}", pptPath, outputFormat);

            if (String.IsNullOrEmpty(outputFormat))
            {
                string errorMessage = "Output format is required.";
                Logger.Error(errorMessage);
                return GenerateErrorResponse(errorMessage);
            }
            if (String.IsNullOrEmpty(pptPath))
            {
                string errorMessage = "PresentationPath to presentation file is required.";
                Logger.Error(errorMessage);
                return GenerateErrorResponse(errorMessage);
            }

            if (outputFormat.ToLower() == "swf")
            {
                return ConvertPptToSwf(pptPath);
            }
            else if (outputFormat.ToLower() == "html")
            {
                return ConvertPptToHtml(pptPath);
            }
            else
            {
                Response response = GenerateErrorResponse("Output format not supported: " + outputFormat);
                Logger.Warn("Error during conversion of {0} to {1}. Error info: {2}", pptPath, outputFormat, response.Message);
                return response;
            }
        }

        

        private Response GenerateErrorResponse(string message)
        {
            Response response = new Response();
            response.Sucess = false;
            response.Message = message;
            return response;
        }


        private Response ConvertPptToSwf(string pptPath)
        {

            Stopwatch stopwatch = Stopwatch.StartNew();

            Logger.Info("Starting PPT to SWF conversion of {0}", pptPath);

            PresentationConvertRequest request = new PresentationConvertRequest(pptPath, PresentationConvertRequest.OutputType.Swf);
            IPresentationConverter converter = PresentationConverterFactory.Get(PresentationConvertRequest.OutputType.Swf);
            PresentationConvertResponse swfResponse = converter.Convert(request);
            Response response = CreateConvertResponse(request, swfResponse);

            stopwatch.Stop();
            long duration = stopwatch.ElapsedMilliseconds;
            Logger.Info(response.Message + "(duration: " + duration + " milisec.)");

            return response;
        }

        private Response ConvertPptToHtml(string pptPath)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            Logger.Info("Starting PPT to HTML conversion of {0}", pptPath);

            PresentationConvertRequest request = new PresentationConvertRequest(pptPath, PresentationConvertRequest.OutputType.Html);
            IPresentationConverter converter = PresentationConverterFactory.Get(PresentationConvertRequest.OutputType.Html);
            PresentationConvertResponse htmlResponse = converter.Convert(request);
            Response response = CreateConvertResponse(request, htmlResponse);

            stopwatch.Stop();
            long duration = stopwatch.ElapsedMilliseconds;
            Logger.Info(response.Message + "(duration: " + duration + " milisec.)");

            return response;
        }


        private Response CreateConvertResponse(PresentationConvertRequest request, PresentationConvertResponse htmlResponse)
        {

            Response response;

            if (htmlResponse.Sucess)
            {
                String message = "File " + request.PresentationPath + " succesfully converted to " + request.Output.ToString();
                var slides = ExtractSlidesInfo(htmlResponse);
                response = (new Response(true, message)).WithPathIn(request.PresentationPath).WithPathOut(htmlResponse.Path).WithOutputType(request.Output.ToString()).WithSlides(slides);
            }
            else
            {
                response = new Response(false, htmlResponse.Message);
            }
            

            return response;
        }

        private static List<Slide> ExtractSlidesInfo(PresentationConvertResponse htmlResponse)
        {
            List<Slide> slides = new List<Slide>();
            foreach (MintraDocumentConverterCore.SlideInfo slideInfo in htmlResponse.Slides)
            {
                Slide slide = new Slide();
                slide.Index = slideInfo.Index;
                slide.File = slideInfo.File;
                slide.Title = slideInfo.Title;
                slides.Add(slide);
            }
            return slides;
        }
    }
}
