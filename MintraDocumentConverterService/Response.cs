﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MintraDocumentConverterService
{
    public class Response
    {
        public Boolean Sucess { get; set; }
        public string Message { get; set; }
        public string PathIn { get; set; }
        public string PathOut { get; set; }
        public string OutputType { get; set; }
        public List<Slide> Slides { get; set; }

        public Response()
        {
        }

        public Response(Boolean sucess, string message)
        {
            this.Sucess = sucess;
            this.Message = message;
        }

        public Response WithPathIn(string pathIn)
        {
            this.PathIn = pathIn;
            return this;
        }

        public Response WithPathOut(string pathOut)
        {
            this.PathOut = pathOut;
            return this;
        }

        public Response WithOutputType(string outputType)
        {
            this.OutputType = outputType;
            return this;
        }

        public Response WithSlides(List<Slide> slides)
        {
            this.Slides = slides;
            return this;
        }
    }
}
