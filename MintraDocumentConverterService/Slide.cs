﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MintraDocumentConverterService
{
    public class Slide
    {
        public int Index { get; set; }
        public string File { get; set; }
        public string Title { get; set; }
    }
}