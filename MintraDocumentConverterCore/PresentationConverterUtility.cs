﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Presentation;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;
using NLog;

namespace MintraDocumentConverterCore
{
    class PresentationConverterUtility
    {
        private static Logger Logger = LogManager.GetCurrentClassLogger();

        public static void ZipAndClean(PresentationConvertRequest request, PresentationConvertResponse response)
        {

            Logger.Debug("Starting zip and clean process ...");

            string startPath = response.Path;
            string zipPath = response.Path + ".zip";

            try
            {
                File.Delete(zipPath);
                ZipFile.CreateFromDirectory(startPath, zipPath, CompressionLevel.Fastest, false);
                response.Path = zipPath;
                Logger.Debug("Folder zipped succesfully to a file {0}.", zipPath);
            }
            catch (Exception e)
            {
                response.Sucess = false;
                response.Message = "Error during zipping: " + e.Message;
                Logger.Error("Error during zipping.", e);
            }


            try
            {
                DirectoryInfo dir = new DirectoryInfo(startPath);
                dir.Delete(true);
                Logger.Debug("Folder {0} deleted.", startPath);
            }
            catch (Exception e)
            {
                Logger.Error("Error during folder removal. Remove it manually: " + startPath, e);
            }

            try
            {
                File.Delete(request.PresentationPath);
                Logger.Debug("Presentation file {0} deleted.", request.PresentationPath);
            }
            catch (Exception e)
            {
                Logger.Error("Error during presentation removal. Remove it manually: " + request.PresentationPath, e);
            }
        }

        public static PresentationConvertResponse GenerateSuccessResponse(PresentationConvertRequest request)
        {
            string outputDirectory = request.GetOutputFolder();
            PresentationConvertResponse response = new PresentationConvertResponse();
            response.Sucess = true;
            response.Message = "File " + request.PresentationPath + " sucessfully convereted to " + request.Output + ". Output folder: " + outputDirectory;
            response.Path = outputDirectory;
            return response;
        }


        public static PresentationConvertResponse GenerateErrorResponse(string message)
        {
            PresentationConvertResponse response = new PresentationConvertResponse();
            response.Sucess = false;
            response.Message = message;
            return response;
        }


        public static string SaveAsPptx(string pptPathIn)
        {

            Microsoft.Office.Interop.PowerPoint.Application presentationApp = new Microsoft.Office.Interop.PowerPoint.Application();
            string pptxPathOut = null;
            try
            {

                string pptDir = Path.GetDirectoryName(pptPathIn);
                string pptFileNameOnly = Path.GetFileNameWithoutExtension(pptPathIn);
                pptxPathOut = Path.Combine(pptDir, pptFileNameOnly + ".pptx");
                presentationApp.Visible = Microsoft.Office.Core.MsoTriState.msoTrue;

                Microsoft.Office.Interop.PowerPoint.Presentations presentations = presentationApp.Presentations;

                Microsoft.Office.Core.MsoTriState readOnly = Microsoft.Office.Core.MsoTriState.msoFalse;
                Microsoft.Office.Core.MsoTriState untitled = Microsoft.Office.Core.MsoTriState.msoFalse;
                Microsoft.Office.Core.MsoTriState withWindow = Microsoft.Office.Core.MsoTriState.msoFalse;

                Logger.Debug("Opening ppt file {0} ...", pptPathIn);
                Microsoft.Office.Interop.PowerPoint.Presentation presentation = presentations.Open(pptPathIn, readOnly,
                    untitled, withWindow);

                Logger.Debug("Starting creation of pptx from ppt {0}", pptPathIn);
                presentation.SaveCopyAs(pptxPathOut, PowerPoint.PpSaveAsFileType.ppSaveAsOpenXMLPresentation,
                    Microsoft.Office.Core.MsoTriState.msoFalse);
                Logger.Debug("Successfully created pptx {0} from ppt {1}", pptxPathOut, pptPathIn);
            }
            catch (Exception e)
            {
                Logger.Error("Error during creating pptx from ppt " + pptPathIn, e);
            }
            finally
            {
                presentationApp.Quit();
            }

            return pptxPathOut;
        }




    }
}
