﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MintraDocumentConverterCore
{
    public class PresentationConvertRequest
    {

        public  enum OutputType
        {
            Swf, Html
        }

        public OutputType Output { get; set; }
        public string PresentationPath { get; set; }

        public PresentationConvertRequest()
        {
        }

        public PresentationConvertRequest(string pptPresentationPath, OutputType outputType)
        {
            this.Output = outputType;
            this.PresentationPath = pptPresentationPath;
        }

        public override string ToString()
        {
            StringBuilder b = new StringBuilder();
            b.Append("PresentationConvertRequest{");
            b.Append("output: " + this.Output.ToString());
            b.Append(", file: " + this.PresentationPath);
            b.Append("}");
            return b.ToString();
        }


        public bool IsPptx()
        {
            string extension = GetPresentationExtension();
            return !String.IsNullOrEmpty(extension) && extension.ToLower() == ".pptx";
        }

        public bool IsPpt()
        {
            string extension = GetPresentationExtension();
            return !String.IsNullOrEmpty(extension) && extension.ToLower() == ".ppt";
        }

        private string GetPresentationExtension()
        {
            return Path.GetExtension(this.PresentationPath);
        }

        public string GetOutputFolder()
        {
            var finalOutLocation = Path.GetDirectoryName(this.PresentationPath);
            var folderName = Path.GetFileNameWithoutExtension(this.PresentationPath);
            if (!string.IsNullOrEmpty(folderName))
            {
                finalOutLocation = Path.Combine(finalOutLocation, folderName);
            }
            return finalOutLocation;
        }


    }
}
