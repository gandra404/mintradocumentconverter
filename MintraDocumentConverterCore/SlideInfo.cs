﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MintraDocumentConverterCore
{
    public class SlideInfo
    {
        public int Index { get; set; }
        public string File { get; set; }
        public string Title { get; set; }
    }
}
