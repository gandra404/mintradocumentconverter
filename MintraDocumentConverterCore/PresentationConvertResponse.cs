﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MintraDocumentConverterCore
{
    public class PresentationConvertResponse
    {
        public Boolean Sucess { get; set; }
        public string Message { get; set; }
        public string Path { get; set; }
        public List<SlideInfo> Slides { get; set; }

        public override string ToString()
        {
            StringBuilder b = new StringBuilder();
            b.Append("PresentationConvertResponse{");
            b.Append("Sucess: " + this.Sucess);
            b.Append(", Path: " + this.Path);
            b.Append(", Message: " + this.Message);
            b.Append("}");
            return b.ToString();
        }

    }

}
