﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MintraDocumentConverterCore
{
    public static class PresentationConverterFactory
    {

        public static IPresentationConverter Get(PresentationConvertRequest.OutputType outputType)
        {
            if (outputType == PresentationConvertRequest.OutputType.Html)
            {
                return PptToHtmlConverter.Instance;
            }

            if (outputType == PresentationConvertRequest.OutputType.Swf)
            {
                return PptToSwfConverter.Instance;
            }

            return null;
        }

    }
}
