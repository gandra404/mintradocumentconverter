﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MintraDocumentConverterCore
{
    public interface IPresentationConverter
    {
        PresentationConvertResponse Convert(PresentationConvertRequest request);
    }
}
