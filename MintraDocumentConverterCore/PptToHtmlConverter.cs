﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using DigitalOfficePro.Html5PointSdk;
using NLog;

namespace MintraDocumentConverterCore 
{
    class PptToHtmlConverter : IPresentationConverter
    {

        private static PptToHtmlConverter _instance = null;
        private static readonly object Padlock = new object();


        private Logger Logger { get; set; }

        private PresentationConverter _presentationConverter;

        private int SlidesCount { get; set; }

        protected PresentationConverter Converter
        {
            get { return _presentationConverter; }  
        }


        private PptToHtmlConverter()
        {
            Logger = LogManager.GetCurrentClassLogger();
        }

        public static PptToHtmlConverter Instance
        {
            get
            {

                lock (Padlock)
                {

                    if (_instance == null)
                    {
                        _instance = new PptToHtmlConverter();
                    }
                    return _instance;
                }
            }
        }



        public PresentationConvertResponse Convert(PresentationConvertRequest request)
        {
            PresentationConvertResponse response;

            Logger.Debug("Receiving request {0}", request);

            if (!File.Exists(request.PresentationPath))
            {
                string errorMessage = "Error during presentation conversion to HTML. File " + request.PresentationPath + " not found.";
                return PresentationConverterUtility.GenerateErrorResponse(errorMessage);
            }


            lock (Padlock)
            {
                response = DoConversion(request);
            }

            if (response.Sucess)
            {
                try
                {
                    SetupSlidesInfo(request, response);
                    PresentationConverterUtility.ZipAndClean(request, response);
                }
                catch(Exception e)
                {
                    response.Sucess = false;
                    response.Message = e.Message;
                }
                
            }

            return response;
        }



        private void SetupSlidesInfo(PresentationConvertRequest request, PresentationConvertResponse response)
        {
            Logger.Debug("Starting extracting slides info ...");

            IList<string> slideTitles = GetSlidesTitles(request);

            response.Slides = new List<SlideInfo>();
            for (int i = 0; i < SlidesCount; i++)
            {
                SlideInfo slideInfo = new SlideInfo();
                slideInfo.Index = (i+1);
                slideInfo.Title = slideTitles[i];
                slideInfo.File = "Slide" + (i + 1) + ".html";
                response.Slides.Add(slideInfo);
            }
        }

        private IList<string> GetSlidesTitles(PresentationConvertRequest request)
        {
            if (request.IsPptx())
            {
                return OpenXmlUtil.GetSlideTitles(request.PresentationPath);
            }

            if (request.IsPpt())
            {
                string pptxPath = PresentationConverterUtility.SaveAsPptx(request.PresentationPath);
                IList<string> titles = OpenXmlUtil.GetSlideTitles(pptxPath);
                try
                {
                    File.Delete(pptxPath);
                    Logger.Debug("Temporary pptx file {0} deleted.", pptxPath);
                }
                catch (Exception e)
                {
                    Logger.Warn("Error deleting file " + pptxPath, e);
                }
                return titles;
            }

            throw new Exception("Not supported presentation type " + request.PresentationPath);
        }


        /// <summary> = i;
        /// To start html5 conversion
        /// </summary>
        private PresentationConvertResponse DoConversion(PresentationConvertRequest request)
        {

            string pptInputFileName = request.PresentationPath;

            //To Initialize conversion, Update settings
            InitalizePresentationConverter();
            UpdateConversionSettings();

            try
            {
                //To Open presentation, Convert, close

                var html5OutputFileName = Path.ChangeExtension(pptInputFileName, "html");

                Logger.Debug("Opening presentation ...");
                Converter.OpenPresentation(pptInputFileName);

                Logger.Debug("Starting conversion ...");
                Converter.Convert(html5OutputFileName);

                /**
                 * TODO Hanging
                 * 
                 * Sometimes hangs here on reading last slide 
                 * When this situation occurs this is the last entry in the log file:
                 * MintraDocumentConverterCore.PptToHtmlConverter.OnReadProgress | Reading Slide 26/26 (100%)
                 * 
                 * This was the initial problem when we stop to use iisexpres nad downgrade .NET v4.5 to v4.0
                 * in order to install it on local IIS
                 * 
                 * Later I have upgraded code to .NET v4.5 and apparently worked but anyway this problem appears from time to time
                 */

                Logger.Debug("Closing presentation ...");
                Converter.ClosePresentation();

            }
            catch (Exception exception)
            {
                string errorMessage = "Error durign conversion of " + pptInputFileName + ". Error: " + exception.Message;
                Logger.Error(errorMessage);
                return PresentationConverterUtility.GenerateErrorResponse(errorMessage);
            }
            finally
            {
                //To release Converter object
                ReleasePresentationConverter();
            }


            return PresentationConverterUtility.GenerateSuccessResponse(request);
        }









        /// <summary>
        /// To initialize Converter object 
        /// </summary>
        private void InitalizePresentationConverter()
        {
            //Initialize PresentationConverter object
            _presentationConverter = new PresentationConverter();
            SlidesCount = 0;
            //Initialize Converter object using user name and product key in case of license purchased user.
            Converter.InitLibrary("userName", "productKey");
            Converter.OnConversionStart += OnConversionStart;
            Converter.OnPresentationReadProgress += OnReadProgress;
            Converter.OnHtml5WriteProgress += OnHtml5WriteProgress;
            Converter.OnConversionEnd += OnConversionEnd;
        }


        /// <summary>
        /// To update conversion settings
        /// </summary>
        private void UpdateConversionSettings()
        {
            var settings = Converter.Settings;
            settings.CreateDirectoryForOutput = true;
            var outputSettings = settings.Output;
            outputSettings.AdvanceOnMouseClick = true;
            //outputSettings.BackgroundColor = Color.White.ToArgb();
            outputSettings.EmbedFonts = true;
            outputSettings.FitToWindow = true;
            outputSettings.IncludeHiddenSlides = false;
            outputSettings.IncludeSlideNotes = false;
            outputSettings.WindowScale = 100;
            outputSettings.SingleHtmlPerSlide = true;
        }


        /// <summary>
        /// To release Converter object's event
        /// </summary>
        private void ReleasePresentationConverter()
        {

            Logger.Debug("Release PresentationConverter object's events ...");
            //Release PresentationConverter object's event
            Converter.OnConversionStart -= OnConversionStart;
            Converter.OnPresentationReadProgress -= OnReadProgress;
            Converter.OnHtml5WriteProgress -= OnHtml5WriteProgress;
            Converter.OnConversionEnd -= OnConversionEnd;
        }



        /// <summary>
        /// Conversion start event
        /// </summary>
        /// <param name="slidesCount"></param>
        private void OnConversionStart(int slidesCount)
        {
            SlidesCount = slidesCount;
            Logger.Debug("Conversion start ... slidesCount: {0}", slidesCount);
        }

        /// <summary>
        /// Read progress event
        /// </summary>
        /// <param name="slideIndex"></param>
        /// <param name="itemIndex"></param>
        /// <param name="readProgress"></param>
        void OnReadProgress(int slideIndex, int itemIndex, float readProgress)
        {
            Logger.Debug("Reading Slide {0}/{1} ({2}%)", slideIndex, SlidesCount, readProgress);
        }

        /// <summary>
        /// write progress event
        /// </summary>
        /// <param name="slideIndex"></param>
        /// <param name="itemIndex"></param>
        /// <param name="writeProgress"></param>
        void OnHtml5WriteProgress(int slideIndex, int itemIndex, float writeProgress)
        {
            Logger.Debug("Writing Slide {0}/{1} ({2}%)", slideIndex, SlidesCount, writeProgress);
        }

        /// <summary>
        /// Conversion end event
        /// </summary>
        /// <param name="status"></param>
        private void OnConversionEnd(ConversionStatus status)
        {
            Logger.Debug("Conversion completed successfully.");
        }




    }
}
