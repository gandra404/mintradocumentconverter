﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MintraDocumentConverterCore
{
    public class ConvertResponse
    {
        public Boolean Sucess { get; set; }
        public string Message { get; set; }
        public string Path { get; set; }
    }
}
