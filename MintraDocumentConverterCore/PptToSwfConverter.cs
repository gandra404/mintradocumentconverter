﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace MintraDocumentConverterCore
{
    class PptToSwfConverter : IPresentationConverter
    {

        private static PptToSwfConverter instance = null;
        private static readonly object Padlock = new object();


        private Logger Logger { get; set; }

        private PptToSwfConverter()
        {
            Logger = LogManager.GetCurrentClassLogger();
        }

        public static PptToSwfConverter Instance
        {
            get
            {
                lock (Padlock)
                {
                    if (instance == null)
                    {
                        instance = new PptToSwfConverter();
                    }
                    return instance;
                }
            }
        }

        public PresentationConvertResponse Convert(PresentationConvertRequest request)
        {
            Logger.Error("PPT to SWF not implemented.");
            return PresentationConverterUtility.GenerateErrorResponse("PPT to SWF not implemented.");
        }




    }
}
